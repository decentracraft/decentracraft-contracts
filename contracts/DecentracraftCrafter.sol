pragma solidity ^0.5.11;

import "./Utils/Ownable.sol";
import "./Utils/IRNGReceiver.sol";

/**
    @title DecentracraftCrafter for crafting NFTs in Decentracraft
    @dev  DecentracraftCrafter
 */
contract DecentracraftCrafter is IRNGReceiver, Ownable{
}
