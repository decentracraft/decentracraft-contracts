pragma solidity ^0.5.11;

// import "./DecentracraftItem.sol";
import "./ERC1155MixedFungible.sol";

/**
    @dev Mintable form of ERC1155
    Shows how easy it is to mint new items
*/
contract Decentracraft is ERC1155MixedFungible  {

    // Contract name
    string public name;
    // Contract symbol
    string public symbol;

    string public contracturi;
    string public tokenbaseuri;

    mapping (uint256 => string) public uriMap;
    
    mapping(address => bool) private _minters; // the contracts allowed to mint

    event IDCreated(uint256 _id);
    event Minter(address bouncer, bool enabled);

    modifier minterOnly() {
      if (msg.sender == owner || _minters[msg.sender] == true) _;
    }

    // DecentracraftItem public decentracraftItem;

    function setContractURI(string memory _contracturi) public ownerOnly {
        contracturi = _contracturi;
    }    

    function setTokenBaseURI(string memory _tokenbaseuri) public ownerOnly {
        tokenbaseuri = _tokenbaseuri;
    }    

    function contractURI() public view returns (string memory) {
        return contracturi;
    }
    
    /// @notice Enable or disable the ability of `minter` to mint tokens (minting rights).
    /// @param minter address that will be given/removed minting rights.
    /// @param enabled set whether the address is enabled or disabled as a minter.
    function setMinter(address minter, bool enabled) external ownerOnly {
        _minters[minter] = enabled;
        emit Minter(minter, enabled);
    }

    /// @notice check whether address `who` is given minter rights.
    /// @param who The address to query.
    /// @return whether the address has minting rights.
    function isMinter(address who) external view returns (bool) {
        return _minters[who];
    }

    constructor (/* DecentracraftItem _dcItem */) public payable  {        
        // decentracraftItem = _dcItem;
    }

    function withdraw() public ownerOnly {
        // decentracraftItem.withdraw();
        msg.sender.transfer(address(this).balance);
    }

    function () external payable {
    }

    uint256 nonce;
    // mapping (uint256 => address) public creators;
    mapping (uint256 => uint256) public maxIndex;

    // modifier creatorOnly(uint256 _id) {
    //     require(creators[_id] == msg.sender);
    //     _;
    // }

    function uri(uint256 _id) external view returns (string memory) {
        // if(isNonFungible(_id)){
        //     return uriMap[_id];//decentracraftItem.uri(_id);
        // }
        return string(abi.encodePacked(tokenbaseuri, uriMap[_id]));
    }

    // solium-disable-next-line security/no-assign-params
    function uint2str(uint256 _i) private pure returns (string memory _uintAsString) {
        if (_i == 0) {
            return "0";
        }

        uint256 j = _i;
        uint256 len;
        while (j != 0) {
            len++;
            j /= 10;
        }

        bytes memory bstr = new bytes(len);
        uint256 k = len - 1;
        while (_i != 0) {
            bstr[k--] = bytes1(uint8(48 + (_i % 10)));
            _i /= 10;
        }

        return string(bstr);
    }

    // This function only creates the type.
    function create(string memory _uri, bool   _isNF) public minterOnly returns(uint256 _type) {
        // Store the type in the upper 128 bits
        _type = (++nonce << 128);

        // Set a flag if this is an NFI.
        if (_isNF)
          _type = _type | TYPE_NF_BIT;

        // This will allow restricted access to creators.
        // creators[_type] = msg.sender;

        if(bytes(_uri).length == 0){
            string memory uristr = string(abi.encodePacked(uint2str(_type), ".json"));
            uriMap[_type] = uristr;
            emit URI(uristr, _type);
        }
        else{
            uriMap[_type] = _uri;
            emit URI(_uri, _type);
        }

        // emit a Transfer event with Create semantic to help with discovery.
        // emit TransferSingle(msg.sender, address(0x0), address(0x0), _type, 0);
        emit IDCreated(_type);

        return _type;
    }
    
    function mintNonFungible(uint256 _type, address[] memory _to, 
                string memory _uri, string memory _attributesJSON) public payable minterOnly returns (uint256 _id)  {

        // No need to check this is a nf type rather than an id since
        // creatorOnly() will only let a type pass through.
        require(isNonFungible(_type));

        // Index are 1-based.
        uint256 index = maxIndex[_type] + 1;
        maxIndex[_type] = _to.length.add(maxIndex[_type]);

        for (uint256 i = 0; i < _to.length; ++i) {
            address dst = _to[i];
            _id  = _type | index + i;

            emit TransferSingle(msg.sender, address(0x0), dst, _id, 1);
            
            addID(_id);
            nfOwners[_id] = dst;
            
            if(bytes(_uri).length == 0){
                string memory uristr = string(abi.encodePacked(uint2str(_id), ".json"));
                uriMap[_id] = uristr;
                emit URI(uristr, _id);
            }
            else{
                uriMap[_type] = _uri;
                emit URI(_uri, _id);
            }

            //decentracraftItem.addDCI.value(msg.value)(id, _uri, _attributesJSON);

            // You could use base-type id to store NF type balances if you wish.
            // balances[_type][dst] = quantity.add(balances[_type][dst]);

            if (dst.isContract()) {
                _doSafeTransferAcceptanceCheck(msg.sender, msg.sender, dst, _id, 1, '');
            }
        }
    }

    function mintFungible(uint256 _id, address[] memory _to, uint256[] memory _quantities) public minterOnly /* creatorOnly(_id) */ {

        require(isFungible(_id));

        addID(_id);
        for (uint256 i = 0; i < _to.length; ++i) {

            address to = _to[i];
            uint256 quantity = _quantities[i];

            // Grant the items to the caller
            balances[_id][to] = quantity.add(balances[_id][to]);

            // Emit the Transfer/Mint event.
            // the 0x0 source address implies a mint
            // It will also provide the circulating supply info.
            emit TransferSingle(msg.sender, address(0x0), to, _id, quantity);

            if (to.isContract()) {
                _doSafeTransferAcceptanceCheck(msg.sender, msg.sender, to, _id, quantity, '');
            }
        }
    }    

    //Emergency function to manually add json hash in case it wasn't handled correctly by provable
    function setDCIJSONHash(uint256 _id, string memory _jsonHash) public ownerOnly{
        // decentracraftItem.setDCIJSONHash(_id, _jsonHash);
    }
}
