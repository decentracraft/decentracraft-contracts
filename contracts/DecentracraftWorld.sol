pragma solidity >=0.5.11;
pragma experimental ABIEncoderV2;

import "./Decentracraft.sol";
import "./DecentracraftCrafter.sol";
import "./Utils/Ownable.sol";
import "./Utils/IRNGReceiver.sol";
import "./Utils/IRandomGenerator.sol";

/**
    @dev Decentracraftworld manager
    Controls minting process
*/
contract DecentracraftWorld is Ownable, IRNGReceiver {

    DecentracraftCrafter[] public craftersList;
    mapping(address => Decentracraft) public tokenCrafters;
    mapping(address => Decentracraft[]) public tokenBurners; 
    mapping(address => address) public craftersRNG;

    mapping (bytes32 => DecentracraftCrafter) randomQueryCrafterMap;
    mapping (bytes32 => address) randomQueryGeneratorMap;

    function getBalance() public view returns (uint256) {
        return address(this).balance;
    }

    function withdraw() public ownerOnly {
        msg.sender.transfer(address(this).balance);
    }

    function () external payable {
    }

    event LogMessage(string message);
    event LogMessage(uint256 message);

    constructor () public payable  { 
    }

    function callTo(address callee, bytes memory data) public ownerOnly returns (bytes memory) {
        (bool success, bytes memory returnData) = callee.call(data);
        assembly {
            if eq(success, 0) {
                revert(add(returnData, 0x20), returndatasize)
            }
        }
        return returnData;
    }

    function updateRNG(address _crafter, address _rng) public ownerOnly {
        craftersRNG[_crafter]   = _rng;
    }

    function updateDCCWorldAddress(address _dccWorldAddress) public ownerOnly {
        for (uint index = 0; index < craftersList.length; index++) {
            craftersList[index].setOwner(_dccWorldAddress);    
            tokenCrafters[address(craftersList[index])].setOwner(_dccWorldAddress);  
            Ownable(craftersRNG[address(craftersList[index])]).setOwner(_dccWorldAddress);                 
        }
    }

    function addTokenCrafter(DecentracraftCrafter _crafter, Decentracraft _token, address _rng) external ownerOnly {
        require(address(tokenCrafters[address(_crafter)]) == address(0));

        craftersList.push(_crafter);
        tokenCrafters[address(_crafter)] = _token;
        craftersRNG[address(_crafter)]   = _rng;
    }

    function removeTokenCrafter(address _crafter, address _newOwner) external ownerOnly {  
        
        for (uint index = 0; index < craftersList.length; index++) {
            if (address(craftersList[index]) == _crafter) {
                craftersList[index].setOwner(_newOwner);    
                delete craftersList[index];
                break;
            }
        }
        tokenCrafters[_crafter].setOwner(_newOwner); 
        delete tokenCrafters[_crafter];

        if(craftersRNG[_crafter] != address(0)){
            Ownable(craftersRNG[_crafter]).setOwner(_newOwner);
        }  
        delete craftersRNG[_crafter];
    }

    function allowToBurn(address _crafter, Decentracraft _decentracraft) external ownerOnly {
        Decentracraft[] storage allowedtokens = tokenBurners[_crafter];
        allowedtokens.push(_decentracraft);
    }

    function allowedToBurn(address _crafter, Decentracraft _decentracraft) public view returns(bool _allowed){
        Decentracraft[] memory allowedtokens = tokenBurners[_crafter];
        for (uint index = 0; index < allowedtokens.length; index++) {
            if(allowedtokens[index] == _decentracraft){
                return true;
            }
        }
        return false;
    }

    function create(string calldata _uri, bool   _isNF) external returns(uint256 _type) {
        return tokenCrafters[msg.sender].create(_uri, _isNF);
    }

    function generateRandom() external returns(bytes32 _queryId){
        bytes32 queryId = IRandomGenerator(craftersRNG[msg.sender]).generateRandom();
        randomQueryCrafterMap[queryId] = DecentracraftCrafter(msg.sender);
        randomQueryGeneratorMap[queryId] = craftersRNG[msg.sender];
        return queryId;
    }

    function craft(uint256 _type, address[] memory _to, string memory _uri, string memory _attributesJSON) public returns(uint256 _id) {
        return tokenCrafters[msg.sender].mintNonFungible(_type, _to, _uri, _attributesJSON);
    }

    function mint(uint256 _id, address[] memory _to, uint256[] memory _quantities) public {
        tokenCrafters[msg.sender].mintFungible(_id, _to, _quantities);
    }

    function __callback(bytes32 _queryId, uint256 _rng) public {
        emit LogMessage("__callback");
        require(msg.sender == randomQueryGeneratorMap[_queryId]);
        require(address(randomQueryCrafterMap[_queryId]) != address(0));        

        randomQueryCrafterMap[_queryId].__callback(_queryId, _rng);
        delete randomQueryCrafterMap[_queryId];
        delete randomQueryGeneratorMap[_queryId];
    } 

    function burn(address _tokenOwner, Decentracraft _decentracraft, uint256 _id, uint256 _amount) public {
        require(tokenCrafters[msg.sender] == _decentracraft || allowedToBurn(msg.sender, _decentracraft));
        _decentracraft.burn(_tokenOwner, _id, _amount);
    }
}
