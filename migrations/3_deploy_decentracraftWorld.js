var DecentracraftWorld = artifacts.require("./DecentracraftWorld.sol");
var MockRNG = artifacts.require("./Utils/MockRNG.sol");

const migration = async (deployer, network, accounts) => {
  await Promise.all([
    deployToken(deployer, network, accounts),
  ]);
};

module.exports = migration;

// ============ Deploy Functions ============


async function deployToken(deployer, network, accounts) {
  await deployer.deploy(DecentracraftWorld);
  // let dccworld = await DecentracraftWorld.deployed();
  // await deployer.deploy(MockRNG);
}
