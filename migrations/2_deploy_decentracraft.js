var Decentracraft = artifacts.require("./Decentracraft.sol");

const migration = async (deployer, network, accounts) => {
  await Promise.all([
    deployToken(deployer, network, accounts),
  ]);
};

module.exports = migration;

// ============ Deploy Functions ============


async function deployToken(deployer, network, accounts) {
  await deployer.deploy(Decentracraft);
  let decentracraft = await Decentracraft.deployed();
  await decentracraft.setMinter(accounts[0], true);
}
