// const Web3 = require('web3');
// const express = require('express');
// const https = require('https');
// const next = require('next')
// var bodyParser = require('body-parser');
// const cors = require('cors');
// const fs = require('fs');
const { waitForEvent } = require('../app/utils/utils');

const Contracts = require('../app/contracts.js');

// Get non-fungible items belonging to user address
module.exports = async function (req, res) {
    
    await Contracts.loadContracts();

    console.log("Entering mintNonFungible");    
    var bodyjson = JSON.parse(req.body);
    // console.log("bodyjson = " + bodyjson);
    var player = bodyjson.player;
    console.log("player = " + player);
    var uri = bodyjson.uri;
    console.log("uri = " + uri);
    var ownerKey = bodyjson.ownerKey;
    console.log("ownerKey = " + ownerKey);

    var decentracraft = await Contracts.Decentracraft;
    
    var createFunction = await decentracraft.methods.create(uri, true);
    var result = await Contracts.sendTransaction(decentracraft, createFunction, ownerKey);
    let typeid = extractTokenID(result);
    console.log("typeid ID = " + typeid);
    
    createFunction = await decentracraft.methods.mintNonFungible(typeid, [player], uri, "");
    result = await Contracts.sendTransaction(decentracraft, createFunction, ownerKey);
    res.json({
        result: result
    });

}


function extractTokenID(tx) {
    for (let l of tx) {
        if (l.event === 'TransferSingle' || l.event === 'IDCreated') {
            return l.args._id;
        }
    }
}

